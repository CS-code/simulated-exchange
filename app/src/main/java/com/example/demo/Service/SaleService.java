package com.example.demo.Service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.example.demo.Bean.Userdata;

import java.util.List;

import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.UpdateListener;

public class SaleService extends Service {
    private SaleBinder mBinder = new SaleBinder();
    private List<Userdata> userdata;
    private String currencyName;
    private float nowPrice;
    private double currencyNum;

    public class SaleBinder extends Binder {
        //买币功能
        public boolean buy() {
            Log.d("OKEX","Buy it");
            float money = 0;
            for(int i = 0; i < userdata.size(); i++) {
                if(userdata.get(i).getId().equals("###")) {
                    money = Float.parseFloat(userdata.get(i).getCount());
                    break;
                }
            }
            if(money < nowPrice * currencyNum)
                return false;
            else {
                for(int i = 0; i < userdata.size(); i++) {
                    if(userdata.get(i).getId().equals("###"))
                        userdata.get(i).setCount(String.valueOf(money - nowPrice * currencyNum));
                    if(userdata.get(i).getId().equals(currencyName)) {
                        double now = Double.parseDouble(userdata.get(i).getCount());
                        userdata.get(i).setCount(String.valueOf(now + currencyNum));
                    }
                    userdata.get(i).update(userdata.get(i).getObjectId(), new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                        }
                    });
                }
            }
            stopSelf();
            return true;
        }
        //卖币功能
        public boolean sell() {
            Log.d("OKEX","Sell it");
            double cnt = 0;
            for(int i = 0; i < userdata.size(); i++) {
                if(userdata.get(i).getId().equals(currencyName)) {
                    cnt = Double.parseDouble(userdata.get(i).getCount());
                    break;
                }
            }
            if(cnt < currencyNum)
                return false;
            else {
                for(int i = 0; i < userdata.size(); i++) {
                    if(userdata.get(i).getId().equals("###")) {
                        float money = Float.parseFloat(userdata.get(i).getCount());
                        userdata.get(i).setCount(String.valueOf(money + nowPrice * currencyNum));
                    }
                    if(userdata.get(i).getId().equals(currencyName)) {
                        userdata.get(i).setCount(String.valueOf(cnt - currencyNum));
                    }
                    userdata.get(i).update(userdata.get(i).getObjectId(), new UpdateListener() {
                        @Override
                        public void done(BmobException e) {
                        }
                    });
                }
            }
            stopSelf();
            return true;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        currencyName = intent.getStringExtra("currencyName");
        currencyNum = Double.parseDouble(intent.getStringExtra("currencyNum"));
        nowPrice = Float.parseFloat(intent.getStringExtra("nowPrice"));
        userdata = (List<Userdata>)intent.getSerializableExtra("userData");
        return mBinder;
    }

    public SaleService() {}

    @Override
    public void onCreate() {
        super.onCreate();
        mBinder = new SaleBinder();
        Log.d("OKEX","Create service.");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("OKEX","Excute service.");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d("OKEX","Stop service.");
        super.onDestroy();
    }
}
