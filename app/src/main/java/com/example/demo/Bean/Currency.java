package com.example.demo.Bean;

import cn.bmob.v3.BmobObject;

public class Currency extends BmobObject {
    private String id;
    private String LastRadio;
    private int imageId;

    public String getLastRadio() { return LastRadio; }

    public void setLastRadio(String lastRadio) { LastRadio = lastRadio; }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
}
