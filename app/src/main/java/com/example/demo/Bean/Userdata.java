package com.example.demo.Bean;

import java.io.Serializable;

import cn.bmob.v3.BmobObject;

public class Userdata extends BmobObject implements Serializable {
    private String username;
    private String id;
    private String count;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
