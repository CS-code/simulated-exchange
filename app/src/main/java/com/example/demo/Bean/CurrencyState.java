package com.example.demo.Bean;

import java.io.Serializable;

import cn.bmob.v3.BmobObject;

public class CurrencyState extends BmobObject implements Serializable {
    private String Date; //日期
    private String Open; //开盘价
    private String High; //最高价
    private String Low; //最低价
    private String Close; //收盘价
    private String Volume; //成交额

    public class A extends CurrencyState {} //A币
    public class B extends CurrencyState {} //B币
    public class C extends CurrencyState {} //C币
    public class D extends CurrencyState {} //D币
    public class E extends CurrencyState {} //E币

    public void setVolume(String volume) {
        Volume = volume;
    }

    public void setOpen(String open) {
        Open = open;
    }

    public void setLow(String low) {
        Low = low;
    }

    public void setHigh(String high) {
        High = high;
    }

    public void setDate(String date) {
        Date = date;
    }

    public void setClose(String close) {
        Close = close;
    }

    public String getDate() {
        return Date;
    }

    public String getClose() {
        return Close;
    }

    public String getHigh() {
        return High;
    }

    public String getLow() {
        return Low;
    }

    public String getOpen() {
        return Open;
    }

    public String getVolume() {
        return Volume;
    }
}
