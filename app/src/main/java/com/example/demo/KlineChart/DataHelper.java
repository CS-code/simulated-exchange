package com.example.demo.KlineChart;

import com.blankj.utilcode.util.TimeUtils;
//import com.example.demo.Bean.A;
import com.example.demo.Bean.CurrencyState;
import com.github.fujianlian.klinechart.KLineEntity;
import com.github.fujianlian.klinechart.formatter.DateFormatter;
import com.github.fujianlian.klinechart.utils.DateUtil;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;

public class DataHelper {

    private List<KLineEntity> kLineEntityList = new ArrayList<>();
    private float mClose = 0, mLowest = 999999, mHighest = 0, mRatio = 0, m24hour = 0;

    public List<KLineEntity> initData(String key, String id) { //暂时只写了一分钟的
        mClose = 0;
        mLowest = 999999;
        mHighest = 0;
        mRatio = 0;
        m24hour = 0;
        if(kLineEntityList.size() != 0) kLineEntityList.clear();
        if(id.equals("A")) {
            BmobQuery<CurrencyState.A> mBmobQuery = new BmobQuery<>();
            mBmobQuery.order("Date"); //按时间排序
            mBmobQuery.findObjects(new FindListener<CurrencyState.A>() {
                @Override
                public void done(List<CurrencyState.A> list, BmobException e) {
                    if (e == null) { //查询成功
                        setkLineEntityList((List<CurrencyState>)(Serializable)list, key);
                    }
                }
            });
        } else if(id.equals("B")) {
            BmobQuery<CurrencyState.B> mBmobQuery = new BmobQuery<>();
            mBmobQuery.order("Date"); //按时间排序
            mBmobQuery.findObjects(new FindListener<CurrencyState.B>() {
                @Override
                public void done(List<CurrencyState.B> list, BmobException e) {
                    if (e == null) { //查询成功
                        setkLineEntityList((List<CurrencyState>)(Serializable)list, key);
                    }
                }
            });
        }
        else if(id.equals("C")) {
            BmobQuery<CurrencyState.C> mBmobQuery = new BmobQuery<>();
            mBmobQuery.order("Date"); //按时间排序
            mBmobQuery.findObjects(new FindListener<CurrencyState.C>() {
                @Override
                public void done(List<CurrencyState.C> list, BmobException e) {
                    if (e == null) { //查询成功
                        setkLineEntityList((List<CurrencyState>)(Serializable)list, key);
                    }
                }
            });
        }
        else if(id.equals("D")) {
            BmobQuery<CurrencyState.D> mBmobQuery = new BmobQuery<>();
            mBmobQuery.order("Date"); //按时间排序
            mBmobQuery.findObjects(new FindListener<CurrencyState.D>() {
                @Override
                public void done(List<CurrencyState.D> list, BmobException e) {
                    if (e == null) { //查询成功
                        setkLineEntityList((List<CurrencyState>)(Serializable)list, key);
                    }
                }
            });
        }
        else if(id.equals("E")) {
            BmobQuery<CurrencyState.E> mBmobQuery = new BmobQuery<>();
            mBmobQuery.order("Date"); //按时间排序
            mBmobQuery.findObjects(new FindListener<CurrencyState.E>() {
                @Override
                public void done(List<CurrencyState.E> list, BmobException e) {
                    if (e == null) { //查询成功
                        setkLineEntityList((List<CurrencyState>)(Serializable)list, key);
                    }
                }
            });
        }
        else{
            List<CurrencyState> list = new ArrayList<>();
            setkLineEntityList(list, key);
        }
        while (kLineEntityList.size() == 0); //等待收到数据
        return kLineEntityList;
    }

    private void setkLineEntityList(List<CurrencyState> list, String key) {
        int cnt = 1, tag = 1;
        float nowOpen = 0, nowHighest = 0, nowLowest = 99999, nowVolume = 0;
        if(key.equals("1min")) tag = 1;
        else if(key.equals("5min")) tag = 5;
        else if(key.equals("15min")) tag = 15;
        else if(key.equals("30min")) tag = 30;
        else if(key.equals("60min")) tag = 60;
        else tag = 1440;
        for(int i = 0; i < list.size(); i++) {
            if(cnt % tag == 0 || i == list.size() - 1) {
                KLineEntity kLineEntity = new KLineEntity();
                kLineEntity.Date = list.get(i).getDate();
                Date date = TimeUtils.string2Date(kLineEntity.Date, DateUtil.strFormat);
                kLineEntity.Date = new DateFormatter().format(date); //格式化
                if(Math.abs(nowOpen) < 0.0001) kLineEntity.Open = Float.parseFloat(list.get(i).getOpen());
                else kLineEntity.Open = nowOpen;
                kLineEntity.Close = Float.parseFloat(list.get(i).getClose());
                if(Float.parseFloat(list.get(i).getHigh()) > nowHighest) kLineEntity.High = Float.parseFloat(list.get(i).getHigh());
                else kLineEntity.High = nowHighest;
                if(Float.parseFloat(list.get(i).getLow()) < nowLowest) kLineEntity.Low = Float.parseFloat(list.get(i).getLow());
                else kLineEntity.Low = nowLowest;
                kLineEntity.Volume = nowVolume + Float.parseFloat(list.get(i).getVolume());
                if(kLineEntity.High > mHighest) mHighest = kLineEntity.High;
                if(kLineEntity.Low < mLowest) mLowest = kLineEntity.Low;
                m24hour += kLineEntity.Volume;
                kLineEntityList.add(kLineEntity);
                cnt = 1;
                nowOpen = 0;
                nowHighest = 0;
                nowLowest = 99999;
                nowVolume = 0;
            } else {
                if(cnt % tag == 1) nowOpen = Float.parseFloat(list.get(i).getOpen());
                if(nowHighest < Float.parseFloat(list.get(i).getHigh()))
                    nowHighest = Float.parseFloat(list.get(i).getHigh());
                if(nowLowest > Float.parseFloat(list.get(i).getLow()))
                    nowLowest = Float.parseFloat(list.get(i).getLow());
                nowVolume += Float.parseFloat(list.get(i).getVolume());
                cnt++;
            }
        }
        mClose = Float.parseFloat(list.get(list.size() - 1).getClose());
        mRatio = (Float.parseFloat(list.get(list.size() - 1).getClose()) - Float.parseFloat(list.get(0).getOpen()))
                / Float.parseFloat(list.get(0).getOpen());
    }

    public List<KLineEntity> getkLineEntityList() {
        return kLineEntityList;
    }

    public String getClose() {
        return String.valueOf(mClose);
    } //收盘价

    public String getHighest() {
        return String.valueOf(mHighest);
    } //最高价

    public String getLowest() {
        return String.valueOf(mLowest);
    } //最低价

    public String get24hour() {
        float all = m24hour / 10000;
        DecimalFormat decimalFormat =new DecimalFormat("0.00");//构造方法的字符格式这里如果小数不足2位,会以0补足.
        String m24hourString = decimalFormat.format(all) + "万";//format 返回的是字符串
        return m24hourString;
    } //24小时成交额

    public String getRatio() {
        NumberFormat percent = NumberFormat.getPercentInstance();
        percent.setMaximumFractionDigits(2);
        String result = percent.format(mRatio).toString();
        if(!result.contains("-")) result = "+" + result;
        return result;
    } //变化率

}
