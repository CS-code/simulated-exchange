package com.example.demo.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.demo.Bean.CurrencyState;
import com.example.demo.Bean.Userdata;
import com.example.demo.R;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;

public class AssetActivity extends AppCompatActivity implements View.OnClickListener {

    private BmobUser bmobUser; //登录用户信息
    private PieChart chart;
    private TextView mA, mB, mC, mD, mE, mF, mAll, mRadio;
    private List<Userdata> userdataList = new ArrayList<Userdata>(); //各种货币信息及初始资金
    private float closeA, closeB, closeC, closeD, closeE;
    private Button reStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset);
        moveStatusbar();
        bmobUser = BmobUser.getCurrentUser(); //初始化用户数据
        Toolbar mToolbarTb = (Toolbar) findViewById(R.id.mtoolbar);
        setSupportActionBar(mToolbarTb);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbarTb.setTitle("资金分布");
        mToolbarTb.setTitleTextColor(Color.BLACK);
        initView();
        initChart();
    }

    private void moveStatusbar() {
        if(Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
            window.setNavigationBarColor(Color.WHITE);
        }
    }

    private void initView() {
        mAll = (TextView) findViewById(R.id.mAll);
        mA = (TextView) findViewById(R.id.mA);
        mB = (TextView) findViewById(R.id.mB);
        mC = (TextView) findViewById(R.id.mC);
        mD = (TextView) findViewById(R.id.mD);
        mE = (TextView) findViewById(R.id.mE);
        mF = (TextView) findViewById(R.id.mF);
        mRadio = (TextView) findViewById(R.id.mradio);
        chart = (PieChart) findViewById(R.id.chart);
        reStart = (Button) findViewById(R.id.reStart);
        reStart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reStart:
                for(int i = 0; i < userdataList.size(); i++) {
                    if(userdataList.get(i).getId().equals("###")) {
                        userdataList.get(i).setCount("5000");
                        userdataList.get(i).update(userdataList.get(i).getObjectId(), new UpdateListener() {
                            @Override
                            public void done(BmobException e) {
                            }
                        });
                    }
                    else {
                        userdataList.get(i).setCount("0");
                        userdataList.get(i).update(userdataList.get(i).getObjectId(), new UpdateListener() {
                            @Override
                            public void done(BmobException e) {
                            }
                        });
                    }
                }
                initChart();
                break;
            default:
                break;
        }
    }

    private void initChart() {
        userdataList = (List<Userdata>)getIntent().getSerializableExtra("userDataList");
        closeA = Float.parseFloat(getIntent().getStringExtra("closeA"));
        closeB = Float.parseFloat(getIntent().getStringExtra("closeB"));
        closeC = Float.parseFloat(getIntent().getStringExtra("closeC"));
        closeD = Float.parseFloat(getIntent().getStringExtra("closeD"));
        closeE = Float.parseFloat(getIntent().getStringExtra("closeE"));
        List<PieEntry> strings = new ArrayList<>();
        float a = 0, b = 0, c = 0, d= 0, e = 0, f = 0;
        for(int i = 0; i < userdataList.size(); i++) {
            if(userdataList.get(i).getId().equals("A")) {
                a = Float.parseFloat(userdataList.get(i).getCount()) * closeA;
                strings.add(new PieEntry(a,"A"));
            }
            else if(userdataList.get(i).getId().equals("B")) {
                b = Float.parseFloat(userdataList.get(i).getCount()) * closeB;
                strings.add(new PieEntry(b,"B"));
            }
            else if(userdataList.get(i).getId().equals("C")) {
                c = Float.parseFloat(userdataList.get(i).getCount()) * closeC;
                strings.add(new PieEntry(c,"C"));
            }
            else if(userdataList.get(i).getId().equals("D")) {
                d = Float.parseFloat(userdataList.get(i).getCount()) * closeD;
                strings.add(new PieEntry(d,"D"));
            }
            else if(userdataList.get(i).getId().equals("E")) {
                e = Float.parseFloat(userdataList.get(i).getCount()) * closeE;
                strings.add(new PieEntry(e,"E"));
            }
            else {
                f = Float.parseFloat(userdataList.get(i).getCount());
                strings.add(new PieEntry(f,"流动资金"));
            }
        }

        DecimalFormat decimalFormat =new DecimalFormat("0.00");
        //String distanceString = decimalFormat.format(distanceValue);
        mAll.setText(decimalFormat.format(a + b + c + d + e + f) + "$");
        mA.setText(decimalFormat.format(a) + "$");
        mB.setText(decimalFormat.format(b) + "$");
        mC.setText(decimalFormat.format(c) + "$");
        mD.setText(decimalFormat.format(d) + "$");
        mE.setText(decimalFormat.format(e) + "$");
        mF.setText(decimalFormat.format(f) + "$");
        float all = a + b + c + d + e + f;
        if(all >= 5000) {
            float radio = (all / 5000 - 1) * 100;
            mRadio.setTextColor(getResources().getColor(R.color.chart_red));
            mRadio.setText("+" + decimalFormat.format(radio) + "%");
        } else {
            float radio = (1 - all / 5000) * 100;
            mRadio.setTextColor(getResources().getColor(R.color.chart_green));
            mRadio.setText("-" + decimalFormat.format(radio) + "%");
        }
        PieDataSet dataSet = new PieDataSet(strings,"");
        ArrayList<Integer> colors = new ArrayList<Integer>();
        colors.add(Color.parseColor("#1296db"));
        colors.add(Color.parseColor("#FFC006"));
        colors.add(Color.parseColor("#d4237a"));
        colors.add(Color.parseColor("#7d670e"));
        colors.add(Color.parseColor("#7adfb8"));
        colors.add(Color.parseColor("#b87adf"));
        dataSet.setColors(colors);
        dataSet.setSliceSpace(3f);

        PieData pieData = new PieData(dataSet);
        pieData.setDrawValues(true);
        pieData.setValueTextSize(15f);
        pieData.setValueTextColor(Color.WHITE);

        chart.setCenterTextSize(22f);

        Legend legend = chart.getLegend();
        legend.setFormSize(18f);
        legend.setTextSize(15f);
        legend.setXEntrySpace(10f);

        chart.setUsePercentValues(true);//允许使用百分比
        chart.getDescription().setEnabled(false);
        chart.setDrawEntryLabels(false);
        chart.setCenterText("资金分布");
        chart.setData(pieData);
        chart.invalidate();//图表重绘
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break; //实现返回
            default: break;
        }
        return true;
    }

}