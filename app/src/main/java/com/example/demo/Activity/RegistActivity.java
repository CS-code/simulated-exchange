package com.example.demo.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.demo.Bean.Userdata;
import com.example.demo.R;

import java.util.Random;

import cn.bmob.v3.BmobUser;
import cn.bmob.v3.exception.BmobException;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;

public class RegistActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText mailbox;
    private EditText username;
    private EditText password;
    private Button mBtnSign;
    private TextView mBtnLogin;

    private void moveStatusbar() {
        if(Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                    | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.WHITE);
            window.setNavigationBarColor(Color.WHITE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);
        moveStatusbar();
        initView();
    }

    void initView() {
        mailbox = (EditText) findViewById(R.id.sign_mailbox);
        username = (EditText) findViewById(R.id.sign_username);
        password = (EditText) findViewById(R.id.sign_password);
        mBtnSign = (Button) findViewById(R.id.mbtn_sign);
        mBtnLogin = (TextView) findViewById(R.id.mbtn_login);
        mBtnLogin.setOnClickListener(this::onClick);
        mBtnSign.setOnClickListener(this::onClick);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mbtn_login :
                Intent intent=new Intent(RegistActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.mbtn_sign :
                String eml = mailbox.getText().toString();
                String usn = username.getText().toString();
                String psw = password.getText().toString();
                if(usn.equals("") || psw.equals("") || eml.equals("")) {
                    Toast.makeText(RegistActivity.this,"用户名和密码及注册邮箱不能为空。",Toast.LENGTH_SHORT).show();
                    return;
                }
                BmobUser user = new BmobUser();
                user.setUsername(usn);
                user.setPassword(psw);
                user.setEmail(eml);
                user.signUp(new SaveListener<Object>() {
                    @Override
                    public void done(Object o, BmobException e) {
                        if(e == null){
                            Toast.makeText(RegistActivity.this,"注册成功，已发送验证邮件至邮箱，确认激活后即可登录",Toast.LENGTH_LONG).show();
                            username.setText("");
                            password.setText("");
                            mailbox.setText("");
                            Userdata tmp = new Userdata();
                            tmp.setUsername(usn);
                            tmp.setCount("0");
                            tmp.setId("A");
                            tmp.save(new SaveListener<String>() {
                                @Override
                                public void done(String s, BmobException e) {
                                }
                            });
                            tmp.setId("B");
                            tmp.save(new SaveListener<String>() {
                                @Override
                                public void done(String s, BmobException e) {
                                }
                            });
                            tmp.setId("C");
                            tmp.save(new SaveListener<String>() {
                                @Override
                                public void done(String s, BmobException e) {
                                }
                            });
                            tmp.setId("D");
                            tmp.save(new SaveListener<String>() {
                                @Override
                                public void done(String s, BmobException e) {
                                }
                            });
                            tmp.setId("E");
                            tmp.save(new SaveListener<String>() {
                                @Override
                                public void done(String s, BmobException e) {
                                }
                            });
                            tmp.setId("###");
                            tmp.setCount("5000");
                            tmp.save(new SaveListener<String>() {
                                @Override
                                public void done(String s, BmobException e) {
                                }
                            });
                        }else{
                            Toast.makeText(RegistActivity.this,"注册失败"+e.getMessage(),Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
            default:
                break;
        }
    }

}